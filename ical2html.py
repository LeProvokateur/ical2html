#!/usr/bin/python

import vobject
import cgi
import os
import logging

EVENT_TEMPLATE = u"""
<html>
<head><title>Event {{{SUMMARY}}}</title></head>
<body>
<h1>{{{SUMMARY}}}</h1>
<p>(which has UID {{{UID}}})</p>
<p><b>When:</b> {{{DTSTART}}} until {{{DTEND}}}</p>
<p><b>Where:</b> {{{LOCATION}}}</p>
<p><b>Description:</b><br />{{{DESCRIPTION}}}</p>
</body>
</html>"""

filename="examples/stratum0-events.ics"
outputdir = "html"

if not os.path.exists(outputdir):
    os.mkdir(outputdir)

f = open(filename, "r")

cal = vobject.readOne(f)

for event in cal.vevent_list:
    uid = ""
    location = ""
    dtstart = ""
    dtend = ""
    summary = ""
    description = ""
    if hasattr(event, "uid"):
        uid = event.uid.value.encode('ascii', 'xmlcharrefreplace')
    else:
        logging.info("Oops, we cannot process this event, it has no UID.")
        continue

    if hasattr(event, "summary"):
        summary = event.summary.value
    else:
        logging.info("Oops, event %d has no summary, ignoring it." % uid)
        continue

    if hasattr(event, "dtstart"):
        dtstart = event.dtstart.value
    else:
        logging.info("Oops, event %d has no start date, ignoring it." % uid)
        continue

    if hasattr(event, "dtend"):
        dtend = event.dtend.value
    else:
        logging.info("Oops, event %d has no end date, ignoring it." % uid)
        continue

    if hasattr(event, "location"):
        location = event.location.value.encode('ascii', 'xmlcharrefreplace')

    if hasattr(event, "description"):
        description = event.description.value

    templ = EVENT_TEMPLATE.replace("{{{UID}}}", cgi.escape(uid).encode('ascii', 'xmlcharrefreplace'))
    templ = templ.replace("{{{SUMMARY}}}", cgi.escape(summary).encode('ascii', 'xmlcharrefreplace'))
    templ = templ.replace("{{{DTSTART}}}", dtstart.strftime("%a, %Y-%m-%d %H:%M"))
    templ = templ.replace("{{{DTEND}}}", dtend.strftime("%a, %Y-%m-%d %H:%M"))
    templ = templ.replace("{{{LOCATION}}}", cgi.escape(location).encode('ascii', 'xmlcharrefreplace'))
    description = description.strip()
    description = cgi.escape(description)
    description = description.replace("\r\n", "\n")
    description = description.replace("\r", "\n")
    description = description.replace("\n", "<br/>")
    templ = templ.replace("{{{DESCRIPTION}}}", description)

    of = open(os.path.join(outputdir, "event-%s.html" % uid), "w")
    of.write(templ)
    of.close()

