#!/usr/bin/python

import vobject
import sys
import os

if len(sys.argv) < 2:
    print """Merge multiple vCalendar/iCalendar files into one.
Syntax: %s [INFILES...] OUTFILE
""" % sys.argv[0]
    sys.exit(1)

inputfiles = sys.argv[1:-1]
outputfile = sys.argv[-1]
outcal = vobject.iCalendar()

for fn in inputfiles:
    f = open(fn)
    cal = vobject.readOne(f)
    for event in cal.vevent_list:
        outcal.add(event)

of = open(outputfile, "w")
of.write(outcal.serialize())
of.close()
