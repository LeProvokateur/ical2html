#!/usr/bin/python

import vobject
import sys
import os
import logging

if len(sys.argv) < 2:
    print """Split a vCalendar/iCalendar file into one file per event.
Give the input file as parameter.
Output will be in the directory <input file>-split.
"""
    sys.exit(1)

inputfile = sys.argv[1]

f = open(inputfile)
cal = vobject.readOne(f)

outputdir = "%s-split" % inputfile
logging.debug("Writing to directory %s" % outputdir)
os.mkdir(outputdir)

for ev in cal.vevent_list:
    ocal = vobject.iCalendar()
    ocal.add(ev)
    of = open(os.path.join(outputdir, "%s.ics" % ev.uid.value), "w")
    of.write(ocal.serialize())
    of.close()
